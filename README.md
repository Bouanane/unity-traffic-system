## Introduction
This a simple traffic system made using Unity and C#, based on the Game Dev Guide Traffic Tutorial.
![System in Action](https://media.giphy.com/media/qyrvAqLqCuhH2qaQCU/giphy.gif)

## Features
- Infinite Waypoints Capability.
- An easy to use Editor Window.
- Sample Scene provided with the code.

## Usage
- Attach the <b>AI.cs</b> script to the GameObject you wish to move.
- Draw the <b>Waypoints</b> using the <b>Waypoints Editor</b> under the <b>Tools</b>.
