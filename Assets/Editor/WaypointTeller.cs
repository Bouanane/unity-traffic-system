﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad()]
public class WaypointTeller : MonoBehaviour
{
    [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Pickable)]
    public static void OnDrawSceneGizmo(waypoint Waypoint, GizmoType gizmoType)
    {
        if ((gizmoType & GizmoType.Selected) != 0)
        {
            Gizmos.color = Color.black;
        }
        else
        {
            Gizmos.color = Color.black * 0.5f;
        }

        Gizmos.DrawSphere(Waypoint.transform.position, 0.3f);
        Gizmos.color = Color.grey;
        Gizmos.DrawLine(Waypoint.transform.position + (Waypoint.transform.right * Waypoint.width / 2f),
            Waypoint.transform.position - (Waypoint.transform.right * Waypoint.width / 2f));

        if (Waypoint.previousWaypoint != null)
        {
            Gizmos.color = Color.red;
            Vector3 offset = Waypoint.transform.right * Waypoint.width / 2f;
            Vector3 offsetTo = Waypoint.previousWaypoint.transform.right * Waypoint.previousWaypoint.width / 2f;

            Gizmos.DrawLine(Waypoint.transform.position + offset, Waypoint.previousWaypoint.transform.position + offsetTo);
        }
        if (Waypoint.nextWaypoint != null)
        {
            Gizmos.color = Color.green;
            Vector3 offset = Waypoint.transform.right * -Waypoint.width / 2f;
            Vector3 offsetTo = Waypoint.nextWaypoint.transform.right * -Waypoint.nextWaypoint.width / 2f;

            Gizmos.DrawLine(Waypoint.transform.position + offset, Waypoint.nextWaypoint.transform.position + offsetTo);
        }
    }
}
 